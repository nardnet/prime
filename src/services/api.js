import axios from "axios";

//Base da Url https://api.themoviedb.org/3
//Base da api https://api.themoviedb.org/3/movie/now_playing?api_key=25a3fd7d09915392e4a4dc09fa104317&language=pt-BR

const api = axios.create({
    baseURL: 'https://api.themoviedb.org/3/'    
});

export default api;


    